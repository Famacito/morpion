package Morpion;

import org.junit.Assert;
import org.junit.Test;

public class TestBoard {

    @Test
    public void testCheckMovesLefttrue(){
        Board board = new Board();
        Assert.assertTrue(board.checkMovesLeft());
    }

    @Test
    public void testCheckMoveLeftFalse(){
        Board board = new Board();
        board.setBoardPosition(0, 'X');
        board.setBoardPosition(4, 'O');
        board.setBoardPosition(8, 'X');
        board.setBoardPosition(1, 'O');
        board.setBoardPosition(7, 'X');
        board.setBoardPosition(6, 'O');
        board.setBoardPosition(2, 'X');
        board.setBoardPosition(5, 'O');
        board.setBoardPosition(3, 'X');
        Assert.assertFalse(board.checkMovesLeft());

    }
}
